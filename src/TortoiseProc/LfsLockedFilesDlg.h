// TortoiseGit - a Windows shell extension for easy version control

// Copyright (C) 2008, 2011-2017 - TortoiseGit
// Copyright (C) 2003-2006,2008 - Stefan Kueng

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
#pragma once

#include "StandAloneDlg.h"
#include "registry.h"
#include "MenuButton.h"
#include "TGitPath.h"
#include "GitStatusListCtrl.h"

/**
* \ingroup TortoiseProc
* Shows the "check for modifications" dialog.
*/
class CLfsLockedFilesDlg : public CResizableStandAloneDialog
{
	DECLARE_DYNAMIC(CLfsLockedFilesDlg)

public:
	CLfsLockedFilesDlg(CWnd* pParent = nullptr);   // standard constructor
	virtual ~CLfsLockedFilesDlg();

	// Dialog Data
	enum { IDD = IDD_LFSLOCKEDFILES };

protected:
	virtual BOOL			OnInitDialog() override;
	virtual void			OnOK() override;
	virtual void			OnCancel() override;
	
	DECLARE_MESSAGE_MAP()

private:
	CMenuButton				m_ctrlUnlock;

};

