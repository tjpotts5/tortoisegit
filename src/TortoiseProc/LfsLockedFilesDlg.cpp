// TortoiseGit - a Windows shell extension for easy version control

// Copyright (C) 2008-2018 - TortoiseGit
// Copyright (C) 2003-2008 - TortoiseSVN

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
#include "stdafx.h"
#include "TortoiseProc.h"
#include "LfsLockedFilesDlg.h"
#include "MessageBox.h"
#include "cursor.h"
#include "AppUtils.h"
#include "ChangedDlg.h"
#include "IconMenu.h"
#include "RefLogDlg.h"

IMPLEMENT_DYNAMIC(CLfsLockedFilesDlg, CResizableStandAloneDialog)
CLfsLockedFilesDlg::CLfsLockedFilesDlg(CWnd* pParent /*=nullptr*/)
	: CResizableStandAloneDialog(CLfsLockedFilesDlg::IDD, pParent)
{
}

CLfsLockedFilesDlg::~CLfsLockedFilesDlg()
{
}

BEGIN_MESSAGE_MAP(CLfsLockedFilesDlg, CResizableStandAloneDialog)
	
END_MESSAGE_MAP()

BOOL CLfsLockedFilesDlg::OnInitDialog()
{
	CResizableStandAloneDialog::OnInitDialog();
	
	UpdateData(FALSE);

	AddAnchor(IDC_LOCKLIST, TOP_LEFT, BOTTOM_RIGHT);
	AddAnchor(IDC_UNLOCK, BOTTOM_RIGHT);
	AddAnchor(IDOK, BOTTOM_RIGHT);
	
	if (hWndExplorer)
		CenterWindow(CWnd::FromHandle(hWndExplorer));
	
	return TRUE;
}

void CLfsLockedFilesDlg::OnOK()
{
	__super::OnOK();
}

void CLfsLockedFilesDlg::OnCancel()
{
	__super::OnCancel();
}
